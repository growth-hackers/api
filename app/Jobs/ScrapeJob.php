<?php

namespace App\Jobs;

use Goutte\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\DomCrawler\Crawler;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PJExport;

class ScrapeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 3;

    protected $data = [];

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recherche = $this->data['recherche'];
        $localisation = $this->data['localisation'];
        $limite = $this->data['limite'];

        $data = array();
        $client = new Client;
        $crash = 0;
        while ($crash < 3) {
            try {
                $crawler = $client->request('GET', 'https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=' . $recherche . '&ou=' . $localisation);
                break;
            } catch (\InvalidArgumentException $e) {
                $crash++;
            }
        }
        $nb_results_get = 0;
        $page = 1;
        $nb_results = $crawler->filter('#SEL-nbresultat')->text();

        while ($nb_results > $nb_results_get) {
            //Page de la liste des articles Pages Jaunes
            $crash = 0;
            while ($crash < 3) {
                try {
                    $crawler = $client->request('GET', 'https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=' . $recherche . '&ou=' . $localisation . '&page=' . $page);
                    break;
                } catch (\InvalidArgumentException $e) {
                    $crash++;
                }
            }

            foreach ($crawler->filter('#listResults article') as $domElement) {
                $domElement = new Crawler($domElement);
                $id = str_replace('bi-bloc-','',$domElement->attr('id'));
                $societe = $adresse = $activite = $siret = $siren = $naf = $effectif = $dirigeant = $site_web = $domaine = $nom = $prenom = $mail = '';

                try {
                    $societe = $domElement->filter('h2')->text();
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $adresse = $domElement->filter('.adresse-container')->text();
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $activite = $domElement->filter('.activites-mentions')->text();
                } catch (\InvalidArgumentException $e) {
                }

                //Page details de l'article Pages Jaunes
                $crawler = $client->request('GET', 'https://www.pagesjaunes.fr/pros/detail?bloc_id='.$id);

                try {
                    $siren = $crawler->filter('.siren')->text();
                    $siren = str_replace('SIREN ','',$siren);
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $siret = $crawler->filter('.siret')->text();
                    $siret = str_replace('SIRET ','',$siret);
                    $siret = substr ($siret,9);
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $naf = $crawler->filter('.naf')->text();
                    $naf = str_replace('Code NAF ','',$naf);
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $effectif = $crawler->filter('.effectif')->text();
                    $effectif = str_replace('Effectif de l\'établissement ','',$effectif);
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    $site_web = $crawler->filter('.lvs-container .value')->text();
                    $domaine = $this->getDomaine($site_web);
                } catch (\InvalidArgumentException $e) {
                }

                try {
                    if(!empty($siren)) {

                        //Page BFM Entreprise
                        $crawler = $client->request('GET', 'https://www.verif.com/societe/'.$siren);
                        try {
                            $dirigeant = $crawler->filter('.dirigeants td')->eq(1)->text();
                        } catch (\InvalidArgumentException $e) {
                        }

                        if (!empty($dirigeant)) {
                            $dirigeant = str_replace('M ','',$dirigeant);
                            $dirigeant = str_replace('Mme ','',$dirigeant);
                            if (count(explode(" ", $dirigeant)) > 1) {
                                $prenom = strtolower(explode(" ", $dirigeant,2)[1]);
                                $prenom = str_replace("'","",$prenom);
                                $prenom = str_replace(" ","",$prenom);
                                $nom = strtolower(explode(" ", $dirigeant,2)[0]);
                                $nom = str_replace("'","",$nom);
                            } else {
                                $nom = strtolower($dirigeant);
                                $prenom = '';
                            }

                            if (!empty($domaine)){
                                $mail = $this->checkEmail($prenom,$nom,$domaine);
                            }
                        }
                    }

                } catch (\InvalidArgumentException $e) {
                }

                if (!empty($societe) && !empty($adresse)) {
                    $prospect = [$societe,$adresse,$activite,$siren,$siret,$naf,$effectif,$dirigeant,$site_web,$domaine,$mail];
                    array_push($data,$prospect);
                    $nb_results_get++;
                    if ($limite <= $nb_results_get){
                        break;
                    }
                }

            }
            $page++;
        }
        $disk = 'local';
        $filename = 'export_'.time().'.xlsx';
        Excel::store(new PJExport($data), $filename, $disk);
        //$fullPath = Storage::disk($disk)->path($filename);
    }

    private function getDomaine($site_web){
        $domaine = str_replace("http://","",$site_web);
        $domaine = str_replace("https://","",$domaine);
        $domaine = str_replace("www.","",$domaine);
        $domaine = str_replace("/","",$domaine);
        return $domaine;
    }

    private function checkEmail($prenom,$nom,$domaine) {
        if (empty($prenom)) {
            $mails = [
                $prenom.'@'.$domaine,
                'syndic@'.$domaine,
                'contact@'.$domaine
            ];
        } else {
            $mails = [
                $prenom.'.'.$nom.'@'.$domaine,
                $prenom.'-'.$nom.'@'.$domaine,
                $prenom.'_'.$nom.'@'.$domaine,
                $prenom.$nom.'@'.$domaine,
                $prenom.'@'.$domaine,
                substr($prenom,0,1).$nom.'@'.$domaine,
                substr($prenom,0,1).'.'.$nom.'@'.$domaine,
                substr($prenom,0,1).'-'.$nom.'@'.$domaine,
                'syndic@'.$domaine,
                'contact@'.$domaine,
            ];
        }

        $client = new Client;

        foreach ($mails as $mail) {
            $mail_b64=base64_encode($mail);
            //Page pour vérifier l'Email
            try {
                $crawler = $client->request('GET', 'http://my-addr.com/email/?ept_mail='.$mail_b64);
                $result = $crawler->filter('.search_result')->html();
            } catch (\InvalidArgumentException $e) {
            }

            if (isset($result)) {
                if (strpos($result,'e-mail exist') != false) {
                    break;
                }

                if ($mail == 'contact@'.$domaine) {
                    if (strpos($result,'e-mail exist') != false) {
                        break;
                    } else {
                        $mail = '';
                    }
                }
            } else {
                $mail = '';
            }
        }

        return $mail;
    }
}
