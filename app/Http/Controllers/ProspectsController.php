<?php

namespace App\Http\Controllers;

use App\Exports\PJExport;
use App\Jobs\ScrapeJob;
use App\Prospect;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\DomCrawler\Crawler;

class ProspectsController extends Controller
{

    public function all () {
        $prospects = Prospect::all();
        return response($prospects,200);
    }
}
