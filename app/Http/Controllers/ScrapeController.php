<?php

namespace App\Http\Controllers;

use App\Exports\PJExport;
use App\Jobs\ScrapeJob;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\DomCrawler\Crawler;

class ScrapeController extends Controller
{

    public function scrape (Request $request) {

        $data = [
            'recherche'=>$request->input('recherche'),
            'localisation'=>$request->input('localisation'),
            'limite'=>$request->input('limite')
        ];

        ScrapeJob::dispatch($data);

        return response('Scraper lancé',200);
    }
}
