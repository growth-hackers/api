<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Prospect extends Model
{
    protected $collection = 'prospects';
    protected $connection = 'mongodb';

}
