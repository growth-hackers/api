<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PJExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    protected $data;

    function __construct($data) {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Agence',
            'Adresse',
            'Activité',
            'SIREN',
            'SIRET',
            'NAF',
            'Effectif',
            'Dirigeant',
            'Site WEB',
            'Domaine',
            'Mail',
        ];
    }

    public function registerEvents(): array
    {
        $styleArray_header = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font'  => [
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 12,
                'name'  => 'Verdana'
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '33313B',
                ]
            ]
        ];

        $styleArray_body = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
        ];


        return [
            AfterSheet::class => function(AfterSheet $event) use ($styleArray_header,$styleArray_body) {
                $nb_rows = count($this->data);
                $cellRange_header = 'A1:K1';
                $cellRange_body = 'A2:K'.($nb_rows+1);
                $event->getSheet()->getDelegate()->getStyle($cellRange_header)->applyFromArray($styleArray_header);
                $event->getSheet()->getDelegate()->getStyle($cellRange_body)->applyFromArray($styleArray_body);
            },
        ];
    }
}
