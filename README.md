<p align="center">
<img src="https://nsa40.casimages.com/img/2020/04/27/200427063309527342.png" width="80">
<img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400">
</p>


<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Scraping
## Le Principe

Outil qui permet d'extraire les infos à partir des PagesJaunes et de BFM entreprise.
- [Pages Jaunes](https://www.pagesjaunes.fr/).
- [BFM Entreprise](https://www.verif.com/recherche/).

Il récupère tout d'abord le nom de la société, l'adresse et l'activité sur la page des articles ([Exemple](https://www.pagesjaunes.fr/annuaire/departement/loire-atlantique-44/syndic-de-copropriete?quoiqui=syndic+de+copropriete&ou=Loire-Atlantique)).
A partir de l'ID de l'article, il se rend sur la page de détails de l'entreprise ([Exemple](pagesjaunes.fr/pros/detail?bloc_id=FCP06632053700001C0001)) afin de récupérer le reste des informations comme le SIREN, l'effectif,... 
Puis à partir du siren il se rend sur la plateforme de BFM ([Exemple](https://www.verif.com/societe/488496084/)) pour récupérer le nom du dirigeant. A l'aide de ce nom et de l'adresse du site web récupérée des tests mails sont lancés pour trouver celle du directeur. 

## L'Installation
Eléments nécessaires en amont :

- PHP
- [Composer](https://getcomposer.org/Composer-Setup.exe)
- [MongoDB](https://www.verif.com/recherche/https://www.mongodb.com/dr/fastdl.mongodb.org/win32/mongodb-win32-x86_64-2012plus-4.2.6-signed.msi/download)
- [Extension PHP Mongo](https://windows.php.net/downloads/pecl/releases/mongodb/1.7.4/php_mongodb-1.7.4-7.2-ts-vc15-x64.zip)

Voir [ce lien](https://www.youtube.com/watch?v=9gEPiIoAHo8&t=60s) pour l'installation, ne pas mettre le "php_" et le ".dll" dans le php.ini

Ne fonctionne pas sur les versions de PHP 7.3 et supérieurs

Pour initialiser le projet il est nécessaire d'installer les composants.

```console
composer install
```

## MongoDB
Installer Mongo DB et Robo 3T

MongoDB installé, créer un dossier data à la racine de votre disque. Dans ce dossier data, créer un dossier db.
Une fois les deux dossiers créés, démarer le serveur mongodb.

Ensuite, lancer Robo 3T. Il faudra créer tout d'abord un utilisateur admin, dans la base de données admin (dossier System). En cochant ces deux rôles : userAdminAnyDatabase et readWriteAnyDatabase.

Créer votre base de données

Arrêter le serveur de BDD et démarer le avec cette commande :
```console
mongod --auth --port 27017
```

Il ne vous  reste plus qu'a renseigner les informations nécessaires à la connection dans la base dans le fichier env. que vous devrez créer à partir du env.example
## Le Fonctionnement
Avant de lancer le serveur pour la première fois
```console
php artisan key:generate 
```

Pour lancer le serveur
```console
php artisan serve
```

Pour lancer le script (le serveur n'a pas besoin d'être lancé)
```console
php artisan scrape:pj
```
